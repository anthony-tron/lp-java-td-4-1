package com.iut.lp;

public class Account {
    private int balance = 100;

    /**
     * Withdraw by @amount
     * @param amount to withdraw
     */
    public void withdraw(int amount) {
        balance -= amount;
    }
}
