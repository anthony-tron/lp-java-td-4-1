Énoncé : https://www.mickael-martin-nevot.com/univ-amu/iut/lp-web/algorithmique-et-uml/s03-td4-1-java-avance.pdf

> Déterminez pourquoi il existe deux manières de faire et quelles sont les différences entre elles.

En Java, il n'y a pas d'héritage multiple donc faire hériter une
classe de *Thread* est une contrainte importante.

Cependant, rien n'interdit une classe d'implémenter plusieurs
interfaces.
