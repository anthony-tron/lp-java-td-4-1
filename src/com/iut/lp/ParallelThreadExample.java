package com.iut.lp;

public class ParallelThreadExample {
    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 1; i <= 26; ++i) {
                System.out.println(i);
            }
        }).start();

        new Thread(() -> {
            for (char i = 'a'; i <= 'z'; ++i) {
                System.out.println(i);
            }
        }).start();
    }
}
